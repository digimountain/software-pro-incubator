Welcome! Your first apprentice job is to improve this course. It doesn't have to be completed right away. You will be presented with the tools and even explicit instructions on how to update this GitLab repository. Any incremental improvement is acceptable as judged by the repository maintainers. Improvements will be accepted through the GitLab merge request mechanism. To be able to do that you could use the built in editor features in GitLab to edit files directly or preferably for practice using the git command line client.

All examples currently only show linux forms of commands but should translate to other os. To get started you must have docker installed to follow the examples. Docker is used to eliminate overhead of services setup and to contain the dependencies outside of your development os.

## Getting Set Up

(Don't worry. I will add details for these. Just trying to get a skeleton first. Ask questions in GitLab issues and I will respond there and copy more details here.)

- Install Linux VM (optional, preferred)
- Install docker (linux) or docker desktop (OSX, Windows)
- Install git
- Install a python editor or ide - suggestions: emacs, pycharm
- Clone this repository
```
git clone git@gitlab.com:digimountain/software-pro-incubator.git
```

- Create an account on GitLab
- Request developer access to this repository
- Create a hello world! to announce yourself in GitLab issues and get used to posting there. Add "introductions" Label.
- Questions get posted here as well. Add the "questions" label.

## First Steps

### Getting used to git

(Don't worry. I will add details for these. Just trying to get a skeleton first)

- Create your personal wip (work-in-progress) branch <name>/wip
- Add your name here .......
- Commit it
- Push it
- View it on GitLab
- Read about GitLab markdown https://docs.gitlab.com/ee/user/markdown.html
- Improve something on this README
- Commit the change
- Create a feature branch
- Cherry pick the improvement commit - skips the yourname commit
- Pull master and rebase to master
- Create a merge request in GitLab
- Respond to comments



- Push your wip branch to back up your work for the day

## Looking Forward

- Getting used to git (continued)


- Database Fundamentals


- Scripting with Python


- Database scripting with python